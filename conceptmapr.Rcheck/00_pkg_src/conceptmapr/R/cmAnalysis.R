########################### License ###########################################################################################################
# R script for Idea Network Analysis (aka Concept mapping) in R to map a network of ideas
# Copyright (C) 2018  Daniel McLinden
#
# The program distributed here is shared in the hope that this will be useful,
# but WITHOUT ANY WARRANTY;without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.
# This work is licensed under the GNU GENERAL PUBLIC LICENSE
################################################################################################################################################

####### NOTE ON VERSION AND CHANGES SINCE LAST VERSION ######################################################################################
# Some lines of script work differently depending on the version of R
# This script was written with the most recent version of R as the time:  [32-bit] C:\Program Files\R\R-3.4.0
# Required is R 3.4.0 or greater.
# Some of the packages (e.g., smacof) have renamed objects in the newer versions of R, this script reflects those name changes.
#    this script is NOT backwards compatible with older versions of R.
# This version of the script has been updated as follows:
# ---- New location of source data and output results is "C:\ideanet"
# ---- Maps in the PowerPoint output have been improved with new colors and better placement of the legend
# ---- Analyses have been added to the Excel output:
# ---- ----- weighted distance centrality indicates the extent to which items were sorted with items in other areas of the map.
#              Low value indicate linking to closer items.
#              High values indicate linking with more distant items.
#              Values are scaled from zero to one.
#              Low values indicate meaning about the location around the point.
#              High values indicate meaning about regions and less about the meaning of the point location.
# ---- ----- Degree centrality is the number of times an item was sorted with other items.
#              High values indicate items that are highly connected.
#              Low values may indicate isolated items.
#
######## Do the following ######################################################################################
# create a folder in the your C drive called "ideanet"
# create an excel file called "input" and save in folder called "ideanet"
# in the file called "input.xlsx" create a worsheet called "matrix" and put the matrix data in this worksheet in the format as described below.
# in the file called "input.xlsx" create a worsheet called "ideas" and put the item number and the item text in the format as described below.
# Before running the program be sure excel files from prior analyses are closed.

###### FUNCTION OF THIS PROGRAM ##############################################################################
# This program will read a square a symmetric summary matrix from a worksheet in input.xlsx and then analyze using MDS
# The program will read in the label worksheet and use the MDS results to determine the location of each label relative
#     to the arithmetic center (mean) of every cluster in all cluster solutions.
#
# Results  output  to output.xlsx
#     One worksheet will be the xy coordinates (dim1,dim2) and cluster membership for each each item.
#     Other worksheets labeled cluster5 through cluster10 can be used to explore the best fitting cluster labels for each cluster.
#
# Results output to output.pptx consist of a B&W point map and a point map color code with a legend
#         for 10 possible cluster solutions from a 5 cluster map to a 15 cluster map.
#
# Results for each cluster output to a file named by the respective cluster number (e.g,Cluster#.pptx).  Once the
#     final cluster solution has been chosen.  Find the similarly named pptx file. This file format is suitable
#     for orienting stakeholders as well as for creating a final and detailed explanation of the results.
#
# Consult the README document for a more detailed explanation of the output.
###########################################################################################################
# The "reshape" R program will take raw data and create a square and symmetric summary matrix of frequencies in a worksheet
#    called "matrix" and a worksheet called "labels."
#
# If you did not use the reshape program then do the following.
# # Create an excel file names as input.xlsx in the working directory specified above
# save the summary matrix in excel as file with the worksheet name "matrix"
# this sheet should be a table with N rows and N columns with no headers
#    10   0   0   6   0   ...
#    0	 10	 1	 0	 0   ...
#    0	 1	 10	 1	 0   ...
#    6	 0	 1	 10	 0   ...
#    0	 0	 0	 0	 10  ...
#
#  Create the "labels" worksheet, see the README for instructions.

# DO THE FOLLOWING IF YOU DID NOT USE A RESHAPE PROGRAM OR DO NOT HAVE WORKSHEET WITH THE TEXT OF EACH ITEM IN THE input.xlsx workbook. Otherwise ignore.
# Create a new worksheet in input.xlsx called 'ideas'
# save the items numbers and text in the "ideas" worksheet in  the following format
#
# item  item_text   --- this is the header in row 1 columns A and B
#   1       text for item 1  -- Data begins in row 2 columns A and B
#   2       text for item 2
#   3       text for item 3
#   ...     ...
#

#' Perform concept mapping analysis.
#'
#' @param cluSolutions cluSolutions Which cluster solutions should be returned? Should be an integer or vector of integers between 1 and total number of items.Deafults to the clustersolutions 5-15.
#' @param inputfile Name of excel-file in working directory containing concept mapping data prepared by,
#' for example, reshapeStacked(). Defaults to "input.xlsx". The file must contain at least three worksheets:
#' 'matrix' containing an N by N table with no columnnames, which for each pair of items gives the number of times the pair have been sorted into the same group;
#' 'ideas' containg a N by 2 table with column names item and item_text, where item is the item number (from 1 to N) and item_text is the corresponding statement;
#' 'labels' containing a table with 4 columns with column names person, item, group and label_ID, where
#' person is an identification number for the sorter, item is the item number, group is the suggested label and label_ID is a unique identification number for each suggested label.
#'
#'
#'
#' @return The function returns a list object where the first element 'df' is a
#' data frame with the following columns: 'dim1', 'dim2' which are xy-coordinates of the items in the concept map;
#' columns containing the cluster solutions (named 'CLUX' where X is the number of clusters in the cluster solution).
#' For each item, 'CLUX' contains the number of the cluster this item is in;
#' 'item' containing item number and 'item_text' containing the item text.
#'
#' @export
#'
#' @examples
#'
cmAnalysis <- function(cluSolutions= c(5:15),
                       inputfile="input.xlsx"
                       ) {

     ##### Load necessary add on packages #####################################################################
     # install packages
     # require(smacof)
     # require(openxlsx)
     ### require(ReporteRs)
     # require(plyr)
     # require(foreach)
     # require(tidyr)
     # require(dplyr)
     # require(RColorBrewer)
     #
     #### Read in data #######################################################################################
     # check if the workbook output.xlsx exists,
     #    if not, then this program will create the output.xlsx
     #    if yes, then this program will delete workbook and which also then removes worksheets so that this program
     #         can write out new data

     # function to delete workbook which then deletes sheets
     del_wb<- function(){
          file.remove(file="output.xlsx")
     }

     # check if file exists, if so do function to delete workbook, if not proceed
     if(file.exists(file="output.xlsx")) del_wb()

     # read in items #change the name of the file in the next line to your filename.xlsx
     items <- openxlsx::read.xlsx(inputfile, colNames = TRUE, sheet="ideas", cols = c(1,2)) ## Ensure two first columns in input.xlsx are the columns with desired content

     # Read in the sorting data #change the name of the file in the next line to your filename.xlsx
     sum_matrix <- as.matrix(openxlsx::read.xlsx(inputfile,
                                       sheet="matrix",colNames=FALSE)) # read sorting data

     # test the input data
     itemrow <- nrow(items)
     matrixrow <- nrow(sum_matrix)
     matrixcol <- ncol(sum_matrix)

     if (matrixrow!=matrixcol) stop("matrix is not square") # test is the matrix is square
     if (itemrow!=matrixcol) stop("mismatch between matrix size and number of items") # test is matrix N is same as number of items

     # prepare the data for MDS
     matrix_dist <- stats::dist(sum_matrix, method = "euclidean") #calc the distance matrix using Euclidean distance

     ##### Start MDS - smacofsym MDS in two dimenisions on a summary matrix ########################################
     metric_mds <- smacof::smacofSym(matrix_dist, ndim = 2) # mds on the distances calculated from the summary matrix
     mconf <- metric_mds$conf #configuration x,y
     mstress <- metric_mds$stress #mds stress
     # note in v 1.9.0 of smacof- confdist is used instead of confdiss as output value in mds()
     # naming in this script is compatible with newest version of smacof - source https://cran.r-project.org/web/packages/smacof/news.html
     metric_confdiss <- metric_mds$confdist #dissimilarity matrix output from the coordinate locations

     #coerce values in object to a dataframe to be used later to create output of this data to excel
     mconf<-as.data.frame(mconf)
     mstress<-as.data.frame(mstress)
     names(mconf)<-c("dim1","dim2") # column names for x,y coordinates

     ##### end MDS #################################################################################################


     ##### Start Cluster Analysis ###################################################################################
     ##### Hierarchical cluster analysis using ward's method on the dissimilarity matrix
     ##### NOTE choose the correct code for ward method based on version
     ##### WARDS METHOD IS WARDS.D2  where version is =>[32-bit] C:\Program Files\R\R-3.2.3

     hiclust <- stats::hclust(metric_confdiss, method="ward.D2") # cluster analysis
     hc_range  <- cluSolutions[order(cluSolutions,decreasing = TRUE)] #15:5 # Identify which cluster solutions to retain for interpretation
     hc_cut<-stats::cutree(hiclust,k=hc_range) # applying cutting so that results included 5 to 15 clusters
     # colnames(hc_cut)<-paste("CLU",hc_range,sep="")#define the column names for clusters

     ##### end Cluster Analysis #####################################################################################

     ##### start network analysis ##################################################################################
     # calculate centrality.  Centrality in this case is defined as the number of people sorting i and j (i.e., the number of ties) together multiplied by the distance between pairs i, j
     # All weighted values for i, j as j are summed and divided by the sum of the ties to calculate a weighted average.
     # Weighted averages can then be compared between items.An item with a low value is connecting with other nearby items.
     # An item with a high value may be connecting with other regions of the map.Low may be an anchor and high may be a bridge.

     dist_table<-as.matrix(matrix_dist) # coerce distance object to a matrix
     sort_dist<-as.data.frame(matrix(nrow=matrixrow,ncol=matrixcol)) # create a data frame to hold data

     # loop through cells and multiply distance*sorts (# since distance in the diagonal is zero multiplication involving diagonal will be zero)
     for (x in 1:matrixcol) # col count
          for (y in 1:matrixrow) # row count
               sort_dist[x,y]<-(sum_matrix[x,y]* dist_table[x,y])


     dist_ties<-colSums(sort_dist)# sum the columns of ties * distance by item
     tie_sums <- colSums(sum_matrix)- sum_matrix[1,1]# sum the number of ties by item, subtracting the value of the diagonal to eliminate reflexive ties.  This is also degree centrality

     # compute the distance weighted centrality sum of ((distance*ties)/(sum of ties -diagonal)) of all ties in a column
     centrality<-matrix(nrow=matrixrow,ncol=1) # create a data frame to hold the data
     centrality_rescale<-matrix(nrow=matrixrow,ncol=1) # create a data frame to hold the data
     for (x in 1:matrixcol)
          centrality[x]<-(dist_ties[x])/(tie_sums[x]) # average weighted distance centrality. Higher values mean more connections with greater distances, lower values mean less distance

     # rescale distance weighted centrality from zero to one.
     denom<-max(centrality)-min(centrality)
     for (x in 1:matrixcol)
          centrality_rescale[x]<-(centrality[x]-min(centrality))/denom

     # Degree centrality is the total ties by item and is the number of times one item was sorted with all other items.
     tie_sums<-as.data.frame(tie_sums) # coerce degree centrality to df for output
     degree_ties<-matrix(nrow=matrixrow,ncol=1)
     for (z in 1:matrixrow)
          degree_ties[z,1]<-tie_sums[z,1]

     # create column names and remove row names
     colnames(centrality)<-"distance weighted centrality"
     colnames(centrality_rescale)<-"rescaled distance centrality"
     colnames(degree_ties)<-"degree centrality"

     ##### End network analysis ###############################################################################

     ##### Start output MDS, CA and Network Analysis objects to Excel #########################################
     output_result_sym<-cbind(mconf,hc_cut,items) #### KAG, AG 180417 ,centrality_rescale,degree_ties) # create datafraime that combines of x,y coordinats; clusters 5-15, item# and item text
     #colnames(hc_cut)<-paste("CLU",hc_range,sep="")#define the column names for clusters
     #colnames(output_result_sym)[!colnames(output_result_sym) %in% c("dim1","dim2","item","item_text")] <- paste0("CLU",colnames(output_result_sym)[!colnames(output_result_sym) %in% c("dim1","dim2","item","item_text")]) #define the column names for clusters
     colnames(output_result_sym)[!colnames(output_result_sym) %in% c("dim1","dim2","item","item_text")] <- paste0("CLU",hc_range)

     wb <- openxlsx::createWorkbook()
     openxlsx::addWorksheet(wb,sheetName = "output")
     openxlsx::writeData(wb,sheet = "output",output_result_sym,colNames = TRUE,rowNames = FALSE)

     openxlsx::addWorksheet(wb,sheetName = "stress")
     openxlsx::writeData(wb,sheet = "stress",mstress,colNames = TRUE,rowNames = FALSE)

     openxlsx::saveWorkbook(wb,"output.xlsx",overwrite = TRUE)

     ##### end output MDS, CA and Network Analysis ############################################################


     ##### Start LABEL ANALYSIS ###############################################################################

     # read in labels worksheet from input.xlsx.  This workheet is created by reshape programs in R
     # or this file and worksheet must be created manually.

     # calculate the mean value for x and y for each label.
     labels <- openxlsx::read.xlsx(inputfile, colNames = TRUE,sheet="labels",cols = 2:4) # load list of labels from stacked or racked output

     xyitems<-cbind(mconf,items) # x,y configuration by item
     xyitems$item_text<-NULL # item text is not needed
     xylabels<-merge(labels, xyitems, by=c("item")) # merge many to one by item
     xylabels<-dplyr::arrange(.data=xylabels, label_ID) # sort by label_ID, not necessary for calculation but makes visual inspection easier
     mean_labels<-plyr::ddply(.data=xylabels, .variables=c("label_ID", "group"), dplyr::summarize, ldim1=mean(dim1), ldim2=mean(dim2)) # labels with dim1 dim2 means


     # # calculate the cluster mean for each cluster in the 5 cluster solution
     clu_means <- list()
     for (cluNo in (cluSolutions[order(cluSolutions,decreasing = FALSE)])) {
          tmpdata <- output_result_sym
          temp_col_name <- paste0("CLU",cluNo)
          tmpdata$tmpvar1 <- tmpdata[,c(temp_col_name)]
          clu_means[[cluNo]] <- plyr::ddply(.data=tmpdata, .variables="tmpvar1", dplyr::summarize, ldim1=mean(dim1), ldim2=mean(dim2)) # cluster centers
     }


     # distance between two points <-sqrt((sum((x2-x1) ^ 2)+((y2-y1) ^ 2))) ##### needs an inner and outer loop with with subscripts
     # function to calculate the distance between labels and cluster centers
     dist_calc<-function(){
          for (out_counter in 1:outer){  # counter for outer loop is number of columns and is equal to number of clusters
               for(in_counter in 1:inner){ # counter for the inner loop is number of rows and is equal to number of cluster labels by label ID
                    # get xy for label, get xy for cluster center and calculate the distance
                    x1<-mean_labels[in_counter,3]   ## $ldim1
                    y1<-mean_labels[in_counter,4]   ## $ldim2
                    x2<-lab_dim1[out_counter,1]
                    y2<-lab_dim2[out_counter,1]
                    df[in_counter, out_counter]<-sqrt((sum((x2-x1) ^ 2)+((y2-y1) ^ 2))) #calc dist
               }
          }
          return(df)
     }

     inner<-nrow(mean_labels) # counter for inner loop of distance function applies to all clusters



     ##### calculates distances beteen center of label and center of cluster
     clusters <- list()
     for (cluNo in (cluSolutions[order(cluSolutions,decreasing = FALSE)])) {
     outer<-nrow(clu_means[[cluNo]])
     in_counter<-1   # reset counter to 1
     out_counter<-1  # reset counter to 1
     df<-as.data.frame(matrix(nrow=inner,ncol=outer)) # create a dataframe to hold the distances
     lab_dim1<-as.data.frame(clu_means[[cluNo]]$ldim1)# create a dataframe of value for the function
     lab_dim2<-as.data.frame(clu_means[[cluNo]]$ldim2)# create a dataframe of value for the function
     # lab_dim1<-as.data.frame(clu5_mean$l5dim1)# create a dataframe of value for the function
     # lab_dim2<-as.data.frame(clu5_mean$l5dim2)# create a dataframe of value for the function
     df<-dist_calc()# run the function
     clusters[[cluNo]] <- cbind(mean_labels, df) # create a dataframe to be output to excel
     rm(df,lab_dim1,lab_dim2) # remove df from memory and reset with new dimensions in next calculation
     }



     # Write worksheets to file ...

     wb <- openxlsx::loadWorkbook(file="output.xlsx")
     for (cluNo in (cluSolutions[order(cluSolutions,decreasing = FALSE)])) {
     openxlsx::addWorksheet(wb,sheetName = paste0("cluster",cluNo))
     openxlsx::writeData(wb,sheet = paste0("cluster",cluNo),clusters[[cluNo]],colNames = TRUE,rowNames = FALSE)
     }
     openxlsx::saveWorkbook(wb,"output.xlsx",overwrite = TRUE)
     message(paste0("Writing output to file 'output.xlsx' in directory ",getwd()))

     return(list("df"=output_result_sym,"lab"=mean_labels,"clu"=clusters))
     ##### End label analysis ##################################################################################

}


