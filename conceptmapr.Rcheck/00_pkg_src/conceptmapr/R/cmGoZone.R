#' Title
#'
#' @param df
#' @param clusterColumnName
#' @param ratingDim1Name
#' @param ratingDim2Name
#' @param ratingDim1NameShow
#' @param ratingDim2NameShow
#' @param rateMinDim1
#' @param rateMinDim2
#' @param rateMaxDim1
#' @param rateMaxDim2
#' @param useQuantile
#' @param label
#' @param useColors
#' @param title
#' @param legend.inset
#' @param legend.cex
#' @param title.cex
#' @param dimName.cex
#' @param showCorr
#' @param suppressLegend
#'
#' @return
#' @export
#'
#' @examples
cmGoZone <- function(df,
                     clusterColumnName="cluster",
                     ratingDim1Name="Important",
                     ratingDim2Name="Feasible",
                     ratingDim1NameShow=ratingDim1Name,
                     ratingDim2NameShow=ratingDim2Name,
                     rateMinDim1=1,
                     rateMinDim2=rateMinDim1,
                     rateMaxDim1=5,
                     rateMaxDim2=rateMaxDim1,
                     useQuantile=0.7,
                     label=df[,clusterColumnName],
                     useColors=NULL,
                     title="Go-Zone",
                     legend.inset=-0.49,
                     legend.cex=0.8,
                     title.cex=1.5,
                     dimName.cex=0.85,
                     showCorr=FALSE,
                     suppressLegend=FALSE)
{

  if(is.null(useColors)){
    useColors <- c(RColorBrewer::brewer.pal(9,"Set1"),RColorBrewer::brewer.pal(6,"Dark2"))
  } else if(!is.null(useColors)) {
    useColors <- useColors
  }

  df$dim1 <- df[,ratingDim1Name]
  df$dim2 <- df[,ratingDim2Name]
  df$cluster <- df[,clusterColumnName]
  df$cluster.f <- as.numeric(as.factor(df$cluster))

  with(df,
       plot(dim1,
            dim2,
            xlab=ratingDim1NameShow,
            ylab=ratingDim2NameShow,
            xlim=c(rateMinDim1,rateMaxDim1),
            ylim=c(rateMinDim2,rateMaxDim2),
            col=useColors[cluster.f],
            pch=20,
            cex=1.8,
            bty="n",
            pty="s")
  )

  abline(v=quantile(df$dim1,p=useQuantile,na.rm=TRUE),col="darkgray",lty=2,lwd=2)
  abline(h=quantile(df$dim2,p=useQuantile,na.rm=TRUE),col="darkgray",lty=2,lwd=2)

  legend("bottomleft",legend = sort(unique(df$cluster)),#levels(valueByItem$cluster.f),
         col=useColors[1:max(df$cluster.f,na.rm=TRUE)],pch=20,bty="n",cex=1,
         pt.cex=1.3)
}
