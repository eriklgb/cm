% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/cmGoZone.R
\name{cmGoZone}
\alias{cmGoZone}
\title{Title}
\usage{
cmGoZone(df, clusterColumnName = "cluster", ratingDim1Name = "Important",
  ratingDim2Name = "Feasible", ratingDim1NameShow = ratingDim1Name,
  ratingDim2NameShow = ratingDim2Name, rateMinDim1 = 1,
  rateMinDim2 = rateMinDim1, rateMaxDim1 = 5, rateMaxDim2 = rateMaxDim1,
  useQuantile = 0.7, label = df[, clusterColumnName], useColors = NULL,
  title = "Go-Zone", legend.inset = -0.49, legend.cex = 0.8,
  title.cex = 1.5, dimName.cex = 0.85, showCorr = FALSE,
  suppressLegend = FALSE)
}
\arguments{
\item{suppressLegend}{}
}
\description{
Title
}
