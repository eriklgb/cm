


data.raw <- read.csv2("FromOptimalSortAllData.csv")

names(data.raw)[names(data.raw)=="Vad.har.du.för.kön."] <- "Kön"
names(data.raw)[names(data.raw)=="Vilken.ålderskategori.tillhör.du."] <- "Åldersgrupp"
names(data.raw)[names(data.raw)=="Inom.vilken.förvaltning.arbetar.du.huvudsakligen...."] <- "Förvaltning"

names(data.raw)[names(data.raw)=="Pre..Vad.har.du.för.yrkesroll..om.du.har.två.yrkesroller.t.ex.chef.och.läkare..välj.den.huvudsakliga.yrkesrollen.du.har.."] <- "Yrkesroll"

usedata <- subset(data.raw,finish.time!="Abandoned")

require(openxlsx)

wb <- createWorkbook()
addWorksheet(wb,sheetName="raw")
# write.xlsx(usedata,"input.xlsx",sheetName="raw",row.names = FALSE)
writeData(wb,sheet = "raw",usedata)



stacked <- usedata
stacked$person <- stacked$participant
stacked$item <- stacked$card.index
stacked$group <- stacked$category.label
stacked <- subset(stacked,select=c("person","item","group"))

addWorksheet(wb,sheetName="stacked")
# write.xlsx(stacked,"input.xlsx",sheetName="stacked",addWorksheet = TRUE,row.names = FALSE)
writeData(wb,sheet = "stacked",stacked)


ideas <- usedata
ideas$item <- ideas$card.index
ideas$item_text <- ideas$card.label
ideas <- unique(subset(ideas,select=c("item","item_text")))
ideas <- ideas[order(ideas$item),]

# write.xlsx(ideas,"input.xlsx",sheetName="ideas",append = TRUE,row.names = FALSE)
addWorksheet(wb,sheetName="ideas")
writeData(wb,sheet="ideas",ideas)

saveWorkbook(wb,"input.xlsx",overwrite = TRUE)

# source('~/Documents/Projekt/CM/workshop/reshape_stacked_data with labels_20171006.R')
# source('~/Documents/Projekt/CM/workshop/conceptmapping 04 21 16_20171006.R')
# 
# idag <- gsub("-","",Sys.Date())
# save.image(paste0("Data/CMRegCancerPlan_",idag,".RData"))


# temp <- output_result_sym
# changeIndex15 <- (1:nrow(temp))[!duplicated(temp$CLU15)]
# changeIndex14 <- (1:nrow(temp))[!duplicated(temp$CLU14)]
# changeIndex13 <- (1:nrow(temp))[!duplicated(temp$CLU13)]
# changeIndex12 <- (1:nrow(temp))[!duplicated(temp$CLU12)]
# changeIndex11 <- (1:nrow(temp))[!duplicated(temp$CLU11)]
# changeIndex10 <- (1:nrow(temp))[!duplicated(temp$CLU10)]
# changeIndex9 <- (1:nrow(temp))[!duplicated(temp$CLU9)]
# changeIndex8 <- (1:nrow(temp))[!duplicated(temp$CLU8)]
# changeIndex7 <- (1:nrow(temp))[!duplicated(temp$CLU7)]
# changeIndex6 <- (1:nrow(temp))[!duplicated(temp$CLU6)]
# changeIndex5 <- (1:nrow(temp))[!duplicated(temp$CLU5)]
# 
# transition15to14 <- temp[temp$CLU14==temp$CLU14[changeIndex15[!changeIndex15 %in% changeIndex14]],]
# transition14to13 <- temp[temp$CLU13==temp$CLU13[changeIndex14[!changeIndex14 %in% changeIndex13]],]
# transition13to12 <- temp[temp$CLU12==temp$CLU12[changeIndex13[!changeIndex13 %in% changeIndex12]],]
# transition12to11 <- temp[temp$CLU11==temp$CLU11[changeIndex12[!changeIndex12 %in% changeIndex11]],]
# transition11to10 <- temp[temp$CLU10==temp$CLU10[changeIndex11[!changeIndex11 %in% changeIndex10]],]
# transition10to9 <- temp[temp$CLU9==temp$CLU9[changeIndex10[!changeIndex10 %in% changeIndex9]],]
# transition9to8 <- temp[temp$CLU8==temp$CLU8[changeIndex9[!changeIndex9 %in% changeIndex8]],]
# transition8to7 <- temp[temp$CLU7==temp$CLU7[changeIndex8[!changeIndex8 %in% changeIndex7]],]
# transition7to6 <- temp[temp$CLU6==temp$CLU6[changeIndex7[!changeIndex7 %in% changeIndex6]],]
# transition6to5 <- temp[temp$CLU5==temp$CLU5[changeIndex6[!changeIndex6 %in% changeIndex5]],]
# 
# transition15to14 <- transition15to14[with(transition15to14,order(CLU14,CLU15)),c("CLU14","CLU15","item","item_text")]
# transition14to13 <- transition14to13[with(transition14to13,order(CLU13,CLU14)),c("CLU13","CLU14","item","item_text")]
# transition13to12 <- transition13to12[with(transition13to12,order(CLU12,CLU13)),c("CLU12","CLU13","item","item_text")]
# transition12to11 <- transition12to11[with(transition12to11,order(CLU11,CLU12)),c("CLU11","CLU12","item","item_text")]
# transition11to10 <- transition11to10[with(transition11to10,order(CLU10,CLU11)),c("CLU10","CLU11","item","item_text")]
# transition10to9 <- transition10to9[with(transition10to9,order(CLU9,CLU10)),c("CLU9","CLU10","item","item_text")]
# transition9to8 <- transition9to8[with(transition9to8,order(CLU8,CLU9)),c("CLU8","CLU9","item","item_text")]
# transition8to7 <- transition8to7[with(transition8to7,order(CLU7,CLU8)),c("CLU7","CLU8","item","item_text")]
# transition7to6 <- transition7to6[with(transition7to6,order(CLU6,CLU7)),c("CLU6","CLU7","item","item_text")]
# transition6to5 <- transition6to5[with(transition6to5,order(CLU5,CLU6)),c("CLU5","CLU6","item","item_text")]
# 
# 
# write.xlsx(transition15to14,"transitions.xlsx",sheetName="15 to 14",row.names=FALSE)
# write.xlsx(transition14to13,"transitions.xlsx",sheetName="14 to 13",row.names=FALSE,append=TRUE)
# write.xlsx(transition13to12,"transitions.xlsx",sheetName="13 to 12",row.names=FALSE,append=TRUE)
# write.xlsx(transition12to11,"transitions.xlsx",sheetName="12 to 11",row.names=FALSE,append=TRUE)
# write.xlsx(transition11to10,"transitions.xlsx",sheetName="11 to 10",row.names=FALSE,append=TRUE)
# write.xlsx(transition10to9,"transitions.xlsx",sheetName="10 to 9",row.names=FALSE,append=TRUE)
# write.xlsx(transition9to8,"transitions.xlsx",sheetName="9 to 8",row.names=FALSE,append=TRUE)
# write.xlsx(transition8to7,"transitions.xlsx",sheetName="8 to 7",row.names=FALSE,append=TRUE)
# write.xlsx(transition7to6,"transitions.xlsx",sheetName="7 to 6",row.names=FALSE,append=TRUE)
# write.xlsx(transition6to5,"transitions.xlsx",sheetName="6 to 5",row.names=FALSE,append=TRUE)
